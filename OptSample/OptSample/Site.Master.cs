﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Optimization;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace OptSample
{
    public partial class SiteMaster : MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            var tag = Styles.Render("~/Content/css/3.0/bundle").ToHtmlString();
            Page.Header.Controls.Add(new Literal { Text = tag });
        }
    }
}